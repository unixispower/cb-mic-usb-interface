/**
 * CB Mic PC Adapter - PTT Board
 * This emulates a single key keyboard that tracks the state of a PTT button.
 */

#include <Keyboard.h>


/*
 * The default key sent on PTT press (`KEYBOARD_KEY`) is 'x' to match the
 * default keybind for certain popular truck sims (ATS, ETS). You can change
 * this to a literal character such as `'a'`, `'1'`, `'~'`, or alternatively
 * use the constants from the Arduino `Keyboard.h` library such as `KEY_F13`,
 * `KEY_RETURN`.
 */
const char KEYBOARD_KEY = 'x';

/*
 * With an unmodified 4 pin mic, the microphone element is connected when the
 * PTT button is pressed. Such connection and disconnection leads to artifacts
 * ("pops") in the audio sampled by the USB soundcard. The window of time
 * between PTT button press and release should match the window between the
 * connection and disconnection of the microphone since they are controlled by
 * the same switch in the handset. However, since the operating system buffers
 * audio, there will likely be a difference in the starting time of the
 * artifact and keyboard event as seen by a program. The mic disconnect
 * artifact will likely occur in the audio stream after the key release which
 * will lead to it being cut out of the audio transmitted by a PTT-aware
 * application. Introducing a delay between the press of the PTT key on the
 * mic and the keypress sent over USB gives time for the mic connect artifact
 * to end before the application starts to transmit audio.
 *
 * With a modified mic there is no need to delay the input to remove audio
 * artifacts as pressing or releasing the PTT key does not disconnect the
 * microphone element.
 *
 * The default press debounce period (`PTT_PRESS_HOLD_MS`) for the PTT button
 * is `150` ms to help eliminate artifacts when using an unmodified mic.
 * Change this value to something lower like `20` if you are using a modified mic.
 */
const unsigned long PTT_PRESS_HOLD_MS = 150;


const unsigned long PTT_RELEASE_HOLD_MS = 1; // time from release to key event
const byte PTT_PIN = 9; // pin that PTT button is attached to


int pttState = HIGH; // pulled LOW when pressed
unsigned long pttStateMs = 0; // timestamp of last ptt state transition


void setup() {
  pinMode(PTT_PIN, INPUT_PULLUP);
  pttState = digitalRead(PTT_PIN);
  pttStateMs = millis();
  Keyboard.begin();
}

void loop() {
  unsigned long now = millis();
  int newPttState = digitalRead(PTT_PIN);

  // button has been pressed or released
  if (newPttState != pttState) {
    // determine hold period to get to next state
    unsigned long holdMs;
    if (newPttState == LOW) {
      holdMs = PTT_PRESS_HOLD_MS;
    } else {
      holdMs = PTT_RELEASE_HOLD_MS;
    }

    // transition to the new state if the hold period has elapsed
    if (now - pttStateMs >= holdMs) {
      pttState = newPttState;
      pttStateMs = now;

      if (newPttState == LOW) {
        Keyboard.press(KEYBOARD_KEY);
      } else {
        Keyboard.release(KEYBOARD_KEY);
      }
    }

  // button state hasn't changed
  } else {
    pttStateMs = now;
  }
}
