# CB Mic USB Interface
A simple Arduino sketch that converts a single switch input to a keypress. This
is part of
[a quick hackup device](https://blaines.world/scraps/cb-mic-usb-interface) that
allows you to use a CB radio mic with computer games and programs.


## Requirements
To build and flash this firmware you will need
[Arduino IDE](https://www.arduino.cc/en/software).


## Hardware
The target board for this project is the
[DFRobot Beetle](https://www.dfrobot.com/product-1075.html), but any ATmega32u4
based Arduino clone should work. See the project page for assembly instructions.


## Flashing
Connect the assembled adapter to your computer and open
`cb-mic-usb-interface.ino` in Arduino IDE. Set the board type to "Leonardo" and
select the serial port the board is attached to. Click "Upload" to compile and
upload the sketch to the adapter.


## Configuration
There are several values that can be customized within the file
`cb-mic-usb-interface.ino`; see comments there for guidance. After making
changes, click "upload" in Arduino IDE to compile and upload the customized
firmware.


## License
Unless otherwise noted the source in this repository is licensed under the
2-clause BSD license, see `LICENSE` for details.

Note: Arduino libraries **statically linked** by this firmware are licensed
under LGPL.
